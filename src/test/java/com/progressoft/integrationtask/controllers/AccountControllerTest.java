package com.progressoft.integrationtask.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.progressoft.integrationtask.entities.Account;
import com.progressoft.integrationtask.services.AccountService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountController.class)
@AutoConfigureMockMvc(addFilters = false)
@Import(TestConfiguration.class)
@ExtendWith(MockitoExtension.class)
class AccountControllerTest {

    // TODO: is this considered unit testing?
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccountService service;
    private AutoCloseable mocks;
    private Account account;

    @BeforeEach
    void setUp() {
        mocks = openMocks(this);
        account  = new Account();
    }

    @Test
    @SneakyThrows
    void givenValidId_whenGetAccountById_thenStatusIsOk() {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/accounts/{id}", 1)
                .accept(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @SneakyThrows
    void whenGetAllAccounts_thenStatusIsOk() {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/accounts")
                .contentType(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @SneakyThrows
    void givenAccount_whenCreateAccount_thenStatusIsOk() {
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/accounts")
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(account));

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @SneakyThrows
    void givenAccount_whenReplaceAccount_thenStatusIsOk() {
        RequestBuilder builder = MockMvcRequestBuilders
                .put("/accounts/{id}", 0)
                .contentType(APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(account));

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @SneakyThrows
    void whenGetForNonExistentApi_thenStatusIsNotFound() {
        RequestBuilder builder = MockMvcRequestBuilders
                .get("/nonExistentApi")
                .accept(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    @SneakyThrows
    void givenEmptyBody_whenReplaceAccount_thenStatusIsBadRequest() {
        RequestBuilder builder = MockMvcRequestBuilders
                .put("/accounts/{id}", 1)
                .contentType(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    @Test
    @SneakyThrows
    void givenEmptyBody_whenCreateAccount_thenStatusIsBadRequest() {
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/accounts")
                .contentType(APPLICATION_JSON);

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    @Test
    @SneakyThrows
    void givenNonExistentEnumValInBody_whenCreateAccount_thenStatusIsBadRequest() {
        RequestBuilder builder = MockMvcRequestBuilders
                .post("/accounts")
                .contentType(APPLICATION_JSON)
                .content("{\n" +
                        "  \"accountCurrency\": \"nonExistentCurrency\"\n" +
                        "}");

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    @Test
    @SneakyThrows
    void givenNonExistentEnumValInBody_whenReplaceAccount_thenStatusIsBadRequest() {
        RequestBuilder builder = MockMvcRequestBuilders
                .put("/accounts/{id}", 1)
                .contentType(APPLICATION_JSON)
                .content("{\n" +
                        "  \"accountCurrency\": \"nonExistentCurrency\"\n" +
                        "}");

        mockMvc.perform(builder)
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
    @AfterEach
    @SneakyThrows
    void cleanup() {
        mocks.close();
    }
}