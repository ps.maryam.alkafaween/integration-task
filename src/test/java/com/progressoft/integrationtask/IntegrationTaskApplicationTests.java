package com.progressoft.integrationtask;

import com.progressoft.integrationtask.controllers.AccountController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class IntegrationTaskApplicationTests {

	@Autowired
	private AccountController controller;

	@Test
	public void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
