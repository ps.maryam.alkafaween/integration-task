package com.progressoft.integrationtask.services;

import com.progressoft.integrationtask.entities.Account;
import com.progressoft.integrationtask.exceptions.AccountNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class AccountServiceTest {
    //TODO: is this the proper way of testing the service, or is it by mocking the underlying repository instead?

    AccountService service;
    Account mockAccount;

    @BeforeEach
    void setUp() {
        service = mock(AccountService.class);
        mockAccount = mock(Account.class);
    }
    @Test
    void givenAnyInteger_whenGetAccountById_thenReturnTypeIsAccountAndMethodIsCalledOnce() {
        when(service.getAccountById(anyInt())).thenReturn(mockAccount);
        Account foundAccount = service.getAccountById(1);
        verify(service, times(1)).getAccountById(1);
        assertThat(foundAccount).isNotNull();
    }
    @Test
    void whenGetAllAccounts_thenReturnTypeIsListOfAccounts() {
        when(service.getAllAccounts()).thenReturn(Collections.singletonList(mockAccount));
        List<Account> accountList = service.getAllAccounts();
        verify(service, times(1)).getAllAccounts();
        assertThat(accountList).isNotNull();
    }
    @Test
    void givenAccountObject_whenCreateAccount_thenReturnTypeIsAccount() {
        when(service.createAccount(any(Account.class))).thenReturn(mockAccount);
        Account newAccount = service.createAccount(mockAccount);
        verify(service, times(1)).createAccount(mockAccount);
        assertThat(newAccount).isNotNull();
    }
    @Test
    void givenAccountObjectAndPathId_whenUpdateAccount_thenReturnTypeIsAccount() {
        when(service.updateAccount(any(Account.class), anyInt())).thenReturn(mockAccount);
        Account updatedAccount = service.updateAccount(mockAccount, 1);
        verify(service, times(1)).updateAccount(mockAccount, 1);
        assertThat(updatedAccount).isNotNull();
    }
    @Test
    void givenNonExistentId_whenGetAccountById_thenAccountNotFoundExceptionIsThrown() {
        when(service.getAccountById(3)).thenThrow(AccountNotFoundException.class);
        assertThrows(AccountNotFoundException.class, () -> service.getAccountById(3));
    }
    @Test
    void givenNonExistentId_whenUpdateAccount_thenAccountNotFoundExceptionIsThrown() {
        when(service.updateAccount(mockAccount, 3)).thenThrow(AccountNotFoundException.class);
        assertThrows(AccountNotFoundException.class, () -> service.updateAccount(mockAccount, 3));
    }
}