package com.progressoft.integrationtask.services;

import com.progressoft.integrationtask.exceptions.AccountNotFoundException;
import com.progressoft.integrationtask.entities.Account;
import com.progressoft.integrationtask.entities.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class AccountService {

    private final AccountRepository repository;

    public AccountService(AccountRepository repository) {
        this.repository = repository;
    }
    public Account getAccountById(@PathVariable int id) {
        return repository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException(id));
    }
    public List<Account> getAllAccounts() {
        return repository.findAll();
    }
    public Account createAccount(@RequestBody Account newAccount) {
        return repository.save(newAccount);
    }
    public Account updateAccount(@RequestBody Account updatedAccount, @PathVariable int id) {

        return repository.findById(id)
                .map(account -> {
                    account.setAccountHolderName(updatedAccount.getAccountHolderName());
                    account.setAccountHolderMobile(updatedAccount.getAccountHolderMobile());
                    account.setAccountHolderAddress(updatedAccount.getAccountHolderAddress());
                    return repository.save(account);
                })
                .orElseThrow(() ->
                    new AccountNotFoundException(id));
    }
}
