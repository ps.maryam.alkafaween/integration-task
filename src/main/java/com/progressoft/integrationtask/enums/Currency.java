package com.progressoft.integrationtask.enums;

public enum Currency {
    JOD, USD, IQD, DJF, EUR
}
