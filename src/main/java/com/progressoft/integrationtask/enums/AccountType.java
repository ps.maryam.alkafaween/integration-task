package com.progressoft.integrationtask.enums;

public enum AccountType {
    CHECKING, SAVINGS, MMA, CD, IRA
}
