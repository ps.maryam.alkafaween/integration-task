package com.progressoft.integrationtask.entities;

import com.progressoft.integrationtask.enums.AccountType;
import com.progressoft.integrationtask.enums.Currency;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Account {
    @Id @GeneratedValue
    private int accountId;
    private String accountHolderName;
    private String accountHolderMobile;
    private String accountHolderAddress;
    private String accountNumber;
    private AccountType accountType;
    private String bankCode;
    private String branchCode;
    private Currency accountCurrency;

    public Account(String accountHolderName, String accountHolderMobile, String accountHolderAddress, String accountNumber, AccountType accountType, String bankCode, String branchCode, Currency accountCurrency) {
        this.accountHolderName = accountHolderName;
        this.accountHolderMobile = accountHolderMobile;
        this.accountHolderAddress = accountHolderAddress;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.bankCode = bankCode;
        this.branchCode = branchCode;
        this.accountCurrency = accountCurrency;
    }
}
