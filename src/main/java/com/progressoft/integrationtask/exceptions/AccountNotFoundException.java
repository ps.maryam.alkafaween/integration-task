package com.progressoft.integrationtask.exceptions;

public class AccountNotFoundException extends RuntimeException{

    public AccountNotFoundException(int accountId) {
        super("Not Found: Account with id " + accountId + " was not found");
    }
}
