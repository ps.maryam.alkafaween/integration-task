package com.progressoft.integrationtask.configs;

import com.progressoft.integrationtask.entities.Account;
import com.progressoft.integrationtask.entities.AccountRepository;
import com.progressoft.integrationtask.enums.AccountType;
import com.progressoft.integrationtask.enums.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
class LoadDatabase {
    AccountRepository repository;

    @Autowired
    public LoadDatabase(AccountRepository repository) {
        this.repository = repository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initDatabase() {
        repository.save(new Account("Ahmad", "0799547843", "my beautiful address", "12345678", AccountType.CD, "BJORJOA", "200", Currency.JOD));
        repository.save(new Account("Umar", "079832932", "his beautiful address", "87654321", AccountType.SAVINGS, "IIBAJOAM", "100", Currency.USD));
    }

}
