package com.progressoft.integrationtask.controllers;
import com.progressoft.integrationtask.services.AccountService;
import com.progressoft.integrationtask.entities.Account;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/accounts")
public class AccountController {
    private final AccountService service;
    public AccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping("/{id}") //TODO: test what happens when you pass a non existent id
    public ResponseEntity<Account> getAccountById(@PathVariable int id) {
        return ResponseEntity.ok(service.getAccountById(id));
    }
    @GetMapping
    public ResponseEntity<List<Account>> getAllAccounts() {
        return ResponseEntity.ok(service.getAllAccounts());
    }
    @PostMapping()
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        return ResponseEntity.ok(service.createAccount(account));
    }
    @PutMapping("/{id}")
    public ResponseEntity<Account> updateAccount(@RequestBody Account account, @PathVariable int id) {
        return ResponseEntity.ok(service.updateAccount(account, id));
    }

}
